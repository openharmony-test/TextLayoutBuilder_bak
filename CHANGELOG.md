## v2.0.1
ArkTs语法适配

## v2.0.0

1. 包管理工具由npm切换成ohpm
2. 适配DevEco Studio 版本：3.1 Beta2(3.1.0.400), SDK: API9 Release(3.2.11.9)

## v1.1.1

- 适配DevEco Studio: 3.1 Beta1(3.1.0.200), SDK: API9 (3.2.10.6)

## v1.1.0

1. 名称由TextLayoutBuilder-ETS修改TextLayoutBuilder。
2. 旧的包@ohos/TextLayoutBuilder-ETS已不维护，请使用新包@ohos/textlayoutbuilder

## v1.0.1

1. 布局调整；
2. api8升级到api9；
3. 处理硬编码问题。

## v1.0.0

- 实现的功能具体如下：

1. 文字字体样式，文字颜色，大小，间距等；

2. 文字排版方向，对齐方式；

3. 副文本样式，点击事件。

- 未实现的功能如下：

1. 文字渐变颜色；

2. 文字行间距；

3. 副文本插入图片。